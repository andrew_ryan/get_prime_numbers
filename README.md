# The get_prime_numbers is the database to store known factorizations for any number. This tool can use on your command line
[![Crates.io](https://img.shields.io/crates/v/loa.svg)](https://crates.io/crates/get_prime_numbers)
[![Rust](https://img.shields.io/badge/rust-1.56.1%2B-blue.svg?maxAge=3600)](https://gitlab.com/andrew_ryan/get_prime_numbers)
[![license](https://img.shields.io/badge/license-MIT-blue.svg)](https://gitlab.com/andrew_ryan/get_prime_numbers/-/raw/master/LICENSE)

## install

```sh
cargo install get_prime_numbers
```

```bash
Usage: get_prime_numbers <number..>
get_prime_numbers 15141 141415151 1714115151
```
