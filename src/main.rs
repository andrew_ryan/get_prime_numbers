use serde::{Deserialize, Serialize};
use reqwest;
use std::sync::{Arc, Mutex};

#[derive(Deserialize,Clone,Debug,Serialize)]
pub struct GetPrimeNumbersResult {
    id: String,
    status: String,
    factors: Vec<(String, i32)>
}
#[derive(Clone,Debug)]
pub struct GetPrimeNumbers {
    n: String,
    result: Option<Arc<Mutex<GetPrimeNumbersResult>>>
}

impl GetPrimeNumbers {
    pub fn new(n: String) -> Self {
        Self {
            n,
            result: None
        }
    }

    pub async fn connect(&mut self, reconnect: bool) -> Result<Arc<Mutex<GetPrimeNumbersResult>>, Box<dyn std::error::Error>> {
        if let Some(result) = &self.result {
            if !reconnect {
                return Ok(result.clone());
            }
        }
        let url = format!("{}?query={}", "http://get_prime_numbers.com/api", self.n.to_string());
        let response = reqwest::get(&url)
            .await?
            .json::<GetPrimeNumbersResult>()
            .await?;

        let res =response;
        self.result = Some(Arc::new(Mutex::new(res.clone())));

        Ok(Arc::new(Mutex::new(res)))
    }

    pub fn get_id(&self) -> Option<String> {
        if let Some(result) = &self.result {
            let locked_result = result.lock().unwrap();
            return Some(locked_result.id.clone());
        }
        None
    }

    pub fn get_status(&self) -> Option<String> {
        if let Some(result) = &self.result {
            let locked_result = result.lock().unwrap();
            return Some(locked_result.status.clone());
        }
        None
    }

    pub fn get_factor_from_api(&self) -> Option<Vec<(String, i32)>> {
        if let Some(result) = &self.result {
            let locked_result = result.lock().unwrap();
            return Some(locked_result.factors.clone());
        }
        None
    }

    pub fn is_prime(&self, include_probably_prime: bool) -> Option<bool> {
        if let Some(result) = &self.result {
            let locked_result = result.lock().unwrap();
            let status = locked_result.status.as_str();
            return Some(status == "P" || (status == "PRP" && include_probably_prime));
        }
        None
    }

    pub fn get_factor_list(&self) -> Vec<String> {
        let factors = self.get_factor_from_api();
        if let Some(factors) = factors {
            return factors.into_iter().flat_map(|(f, _cnt)| vec![f.to_string()]).collect();
        }
        vec![]
    }
}
#[tokio::main]
async fn main(){
    use doe::*;
    let args = args!();
    if args.len()<1{
        println!("Usage: get_prime_numbers <number..>");
    }else{
        for num in args.iter() {
            let mut db = GetPrimeNumbers::new(num.to_string());
            let _result = db.connect(true).await;
            let _status = db.get_status();
            let _id = db.get_id();
            let _factors = db.get_factor_list();
            let _is_prime = db.is_prime(true);
            println!("{}", _factors.join(" "));
        }
    }
}